package com.toriatec.firman.celengan;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        this.checkSession();
    }

    private void checkSession() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                openActivity(MainActivity.class);
            }
        }, 2000);
    }

    private void openActivity(Class<?> clazz) {
        startActivity(new Intent(SplashActivity.this, clazz));
        finish();
    }

}
