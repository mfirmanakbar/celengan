package com.toriatec.firman.celengan;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;

import com.toriatec.firman.celengan.db.DbCelengan;
import com.toriatec.firman.celengan.util.NumberTextWatcher;

public class EditActivity extends AppCompatActivity {

    private EditText mtxtNominal, mKeterangan;
    private String kategori="";
    private RadioButton mrbPemasukan, mrbPengeluaran;
    private ImageView img1, img2, img3;
    String idx, kategorix, ketx, nominalx;
    String nominalnya;
    String nominalnyass;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit);

        mtxtNominal = (EditText) findViewById(R.id.txtNominals);
        mKeterangan = (EditText) findViewById(R.id.txtKet);

        mtxtNominal.addTextChangedListener(new NumberTextWatcher(mtxtNominal));

        mrbPemasukan = (RadioButton) findViewById(R.id.rbPemasukan);
        mrbPengeluaran = (RadioButton) findViewById(R.id.rbPengeluaran);
        img1 = (ImageView) findViewById(R.id.imgTambah1);
        img2 = (ImageView) findViewById(R.id.imgTambah2);
        img3 = (ImageView) findViewById(R.id.imgTambah3);

        idx = getIntent().getStringExtra("idx");
        nominalx = getIntent().getStringExtra("nominalx");
        kategorix = getIntent().getStringExtra("kategorix");
        ketx = getIntent().getStringExtra("ketx");

        Log.d("put_nya", idx + nominalx + kategorix + ketx);

        putResult();

    }

    private void putResult() {
        mtxtNominal.setText(String.valueOf(nominalx));
        mKeterangan.setText(ketx);
        if(kategorix.equals("Pemasukan")){
            mrbPemasukan.setChecked(true);
            kategori = "Pemasukan";
        }else if (kategorix.equals("Pengeluaran")){
            mrbPengeluaran.setChecked(true);
            kategori = "Pengeluaran";
        }
    }

    public void onBtnNominalClears(View view){
        mtxtNominal.setText("");
    }

    public void onBtnCancelClickeds(View view){ finish();}

    public void onRadioButtonClicked2(View view) {
        boolean checked = ((RadioButton) view).isChecked();
        switch(view.getId()) {
            case R.id.rbPemasukan:
                if (checked)
                    kategori = "Pemasukan";
                    img1.setColorFilter(ContextCompat.getColor(getApplicationContext(),R.color.colorGreen));
                    img2.setColorFilter(ContextCompat.getColor(getApplicationContext(),R.color.colorGreen));
                    img3.setColorFilter(ContextCompat.getColor(getApplicationContext(),R.color.colorGreen));
                    mrbPemasukan.setError(null);
                    mrbPengeluaran.setError(null);
                break;
            case R.id.rbPengeluaran:
                if (checked)
                    kategori = "Pengeluaran";
                    img1.setColorFilter(ContextCompat.getColor(getApplicationContext(),R.color.colorRed));
                    img2.setColorFilter(ContextCompat.getColor(getApplicationContext(),R.color.colorRed));
                    img3.setColorFilter(ContextCompat.getColor(getApplicationContext(),R.color.colorRed));
                    mrbPemasukan.setError(null);
                    mrbPengeluaran.setError(null);
                break;
        }
    }

    public void onBtnUpdateClicked(View view){

        String saldo_now = MainActivity.saldo_now;
        int saldo_now_int = Integer.valueOf(saldo_now);
        Log.d("saldo_now_fix", String.valueOf(saldo_now_int));
        String nominalnyas = mtxtNominal.getText().toString();//.replace(".", "");
        nominalnyass = nominalnyas.replace(".", "");
        nominalnyass = nominalnyass.replace(",","");
        Log.d("pess_edit",nominalnyas + " & " + nominalnyass);
        if(nominalnyass.equals("")){
            mtxtNominal.setError("Nominal kosong!");
        }else if (kategori.isEmpty()){
            mrbPemasukan.setError("Pilih kategori!");
            mrbPengeluaran.setError("Pilih kategori!");
        }
        else {
            Integer nominal_long=0;
            if(nominalnyass!=""){
                nominal_long= Integer.valueOf(nominalnyass);
            }

            if(nominal_long>saldo_now_int && kategori == "Pengeluaran"){
                Log.e("validasi", "tes");
                mtxtNominal.setError("Celengan kamu tidak cukup untuk diambil sebesar" +nominalnyass+" !");
            }else {
                AlertDialog.Builder builder = new AlertDialog.Builder(EditActivity.this);
                DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which){
                            case DialogInterface.BUTTON_POSITIVE:
                                update(kategori, Integer.parseInt(nominalnyass), mKeterangan.getText().toString());
                                break;

                            case DialogInterface.BUTTON_NEGATIVE:
                                //No button clicked
                                break;
                        }
                    }
                };
                builder.setMessage("Kamu yakin memperbarui data ini?").
                        setPositiveButton("Ya", dialogClickListener).
                        setNegativeButton("Tidak", dialogClickListener).
                        show();
            }
        }

        //OLD
//        String saldo_now = MainActivity.saldo_now;
//        int saldo_now_int = Integer.valueOf(saldo_now);
//        Log.d("saldo_now_fix", String.valueOf(saldo_now_int));
//        nominalnya = mtxtNominal.getText().toString().replaceAll(",","");
//        Integer nominal_long=0;
//        if(nominalnya!=""){
//            nominal_long= Integer.valueOf(nominalnya);
//        }
//        if(nominalnya.equals("")){
//            mtxtNominal.setError("Nominal kosong!");
//        }else if (kategori.isEmpty()){
//            mrbPemasukan.setError("Pilih kategori!");
//            mrbPengeluaran.setError("Pilih kategori!");
//        }else if(nominal_long>saldo_now_int && kategori == "Pengeluaran"){
//            Log.e("validasi", "tes");
//            mtxtNominal.setError("Celengan kamu tidak cukup untuk diambil sebesar" +nominalnya+" !");
//        }
//        else {
//            AlertDialog.Builder builder = new AlertDialog.Builder(EditActivity.this);
//            DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
//                @Override
//                public void onClick(DialogInterface dialog, int which) {
//                    switch (which){
//                        case DialogInterface.BUTTON_POSITIVE:
//                            int nominalnyas = Integer.valueOf(nominalnya);
//                            update(kategori, nominalnyas, mKeterangan.getText().toString());
//                            break;
//
//                        case DialogInterface.BUTTON_NEGATIVE:
//                            //No button clicked
//                            break;
//                    }
//                }
//            };
//            builder.setMessage("Kamu yakin memperbarui data ini?").
//                    setPositiveButton("Ya", dialogClickListener).
//                    setNegativeButton("Tidak", dialogClickListener).
//                    show();
//        }
    }

    private void update(String kategori, int nominalnyas, String kets) {
        //Log.d("edit_data", "masuk dulu" + kategori + nominalnyas + kets);
        DbCelengan db = new DbCelengan(getApplicationContext());
        db.open();
        db.updateCelengan(idx, kategori, nominalnyas, kets);
        db.close();
        finish();
    }

    public void onBtnDeleteClicked(View view){

        AlertDialog.Builder builder = new AlertDialog.Builder(EditActivity.this);
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which){
                    case DialogInterface.BUTTON_POSITIVE:
                        hapus_data();
                        break;

                    case DialogInterface.BUTTON_NEGATIVE:
                        //No button clicked
                        break;
                }
            }
        };
        builder.setMessage("Kamu yakin menghapus data ini?").
                setPositiveButton("Ya", dialogClickListener).
                setNegativeButton("Tidak", dialogClickListener).
                show();

    }

    private void hapus_data() {
        DbCelengan db = new DbCelengan(getApplicationContext());
        db.open();
        db.deleteCelengan(idx);
        db.close();
        finish();
    }
}
