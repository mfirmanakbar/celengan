package com.toriatec.firman.celengan;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.toriatec.firman.celengan.adapter.AdapterCelengan;
import com.toriatec.firman.celengan.data.DataCelengan;
import com.toriatec.firman.celengan.db.DbCelengan;
import com.toriatec.firman.celengan.util.RecyclerItemClickListener;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Locale;

public class MainActivity extends AppCompatActivity {

    private static RecyclerView.Adapter adapter;
    private RecyclerView.LayoutManager layoutManager;
    private static RecyclerView recyclerView;
    private static ArrayList<DataCelengan> data = new ArrayList<DataCelengan>();
    public static String sum_masuk="0", sum_keluar="0";
    double sum_sekarang=0, persen_masuk=0, persen_keluar=0;
    TextView mtxtCelenganSekarang, mtxtCelenganPengeluaran, mtxtCelenganPemasukan, txtPersenM, txtPersenK;
    private ProgressBar mProgress;
    public static String saldo_now;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent a = new Intent(getApplicationContext(), TambahActivity.class);
                startActivity(a);
                //Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG).setAction("Action", null).show();
            }
        });

        mProgress = (ProgressBar) findViewById(R.id.MyprogressBar);
        mtxtCelenganSekarang = (TextView) findViewById(R.id.txtCelenganSekarang);
        mtxtCelenganPemasukan = (TextView) findViewById(R.id.txtCelenganPemasukan);
        mtxtCelenganPengeluaran = (TextView) findViewById(R.id.txtCelenganPengeluaran);
        txtPersenM = (TextView) findViewById(R.id.txtPersenMasuk);
        txtPersenK = (TextView) findViewById(R.id.txtPersenKeluar);

        recyclerView = (RecyclerView) findViewById(R.id.recycler_view_main);
        recyclerView.setHasFixedSize(true);

        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        adapter = new AdapterCelengan(data);
        recyclerView.setAdapter(adapter);

        refresh_views();
        sum_nominal();
        setTextNominal();
    }

    @Override
    protected void onStart() {
        super.onStart();
        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recycler_view_main);
        recyclerView.addOnItemTouchListener(new RecyclerItemClickListener(this, new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View childView, int position) {
                final String idx = data.get(position).getId();
                final String nominalx = String.valueOf(data.get(position).getNominal());
                final String kategorix = data.get(position).getKategori();
                final String ketx = data.get(position).getKeterangan();
                Log.d("klik_nih", idx + " - " + kategorix + " - " + nominalx + " - " + ketx);

                Intent i = new Intent(MainActivity.this, EditActivity.class);
                i.putExtra("idx", idx);
                i.putExtra("nominalx", nominalx);
                i.putExtra("kategorix", kategorix);
                i.putExtra("ketx", ketx);
                startActivity(i);
            }

            @Override
            public void onItemLongPress(View childView, int position) {

            }
        }));

    }

    private void setTextNominal() {
        mtxtCelenganSekarang.setText("Celenganmu: Rp "+ NumberFormat.getInstance(Locale.GERMANY).format(Double.parseDouble(String.valueOf(sum_sekarang))));
        mtxtCelenganPemasukan.setText("Rp "+ NumberFormat.getInstance(Locale.GERMANY).format(Double.parseDouble(sum_masuk)));
        mtxtCelenganPengeluaran.setText("Rp "+ NumberFormat.getInstance(Locale.GERMANY).format(Double.parseDouble(sum_keluar)));
        String pm = String.valueOf((new Double(persen_masuk)).longValue())+"%";
        String pk = String.valueOf((new Double(persen_keluar)).longValue())+"%";
        if (sum_sekarang<=0){
            pm="0%";
            pk="0%";
        }
        txtPersenM.setText(pm);
        txtPersenK.setText(pk);
        int a = (new Double(persen_masuk)).intValue();
        if(sum_sekarang<=0){
            a=0;
            mProgress.setProgress(a);
        }else {
            mProgress.setProgress(a);
        }
    }

    private void sum_nominal() {
        DbCelengan db = new DbCelengan(getApplicationContext());
        db.open();
        sum_masuk = String.valueOf(db.sum_nominal("Pemasukan"));
        sum_keluar = String.valueOf(db.sum_nominal("Pengeluaran"));
        sum_sekarang = Long.parseLong(sum_masuk)-Long.parseLong(sum_keluar);
        persen_masuk = round(Double.parseDouble(sum_masuk)/(Long.parseLong(sum_masuk)+Long.parseLong(sum_keluar))*100);
        persen_keluar = 100 - persen_masuk;
        Log.d("saldo_now", "Ya");
        saldo_now = String.valueOf((new Double(sum_sekarang)).longValue());
        Log.d("saldo_now", String.valueOf((new Double(sum_sekarang)).longValue()));
        db.close();
    }

    private int round(double d){
        /*
        1.4>1
        1.6>2
        -2.1>-2
        -1.3>-1
        -1.5>-2
        */
        double dAbs = Math.abs(d);
        int i = (int) dAbs;
        double result = dAbs - (double) i;
        if(result<0.5){
            return d<0 ? -i : i;
        }else{
            return d<0 ? -(i+1) : i+1;
        }
    }

    private void refresh_views() {
        DbCelengan db = new DbCelengan(getApplicationContext());
        db.open();
        Cursor cur = db.getCelengan();
        if(cur.getCount()!=0){
            DbCelengan db2 = new DbCelengan(getApplicationContext());
            db2.open();
            Cursor cur2 = db.getCelengan();
            if (cur2 != null) {
                if (cur2.moveToFirst()) {
                    if (cur2.getCount() != 0) {
                        do {
                            DataCelengan item = new DataCelengan();
                            item.setId(cur2.getString(0));
                            item.setTanggal(cur2.getString(1));
                            item.setKategori(cur2.getString(2));
                            item.setNominal(cur2.getInt(3));
                            item.setKeterangan(cur2.getString(4));
                            data.add(item);
                            Log.d("hani_f5", cur2.getString(0) +"_"+ cur2.getString(1)+"_"+cur2.getString(2)+"_"+cur2.getString(3)+"_"+cur2.getString(4));
                        } while (cur2.moveToNext());
                    } else {
                    }
                }
            } else {
            }
            db.close();

        }else {
        }
        db.close();
    }

    @Override
    protected void onResume() {
        super.onResume();
        data.clear();
        adapter.notifyDataSetChanged();
        refresh_views();
        sum_nominal();
        setTextNominal();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_refresh) {
            data.clear();
            adapter.notifyDataSetChanged();
            refresh_views();
            sum_nominal();
            setTextNominal();
        }else if(id == R.id.info_apps){
            Intent intent = new Intent(MainActivity.this, InfoAppActivity.class);
            startActivity(intent);
        }
        return super.onOptionsItemSelected(item);
    }

}
