package com.toriatec.firman.celengan.db;


import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class DbCelengan {
	private SQLiteDatabase db;
	private final Context con;
	private final DbOpenHelper dbHelper;

	public DbCelengan(Context con) {
		this.con = con;
		dbHelper = new DbOpenHelper(this.con, "", null, 0);
	}
	
	public void open() {
		db = dbHelper.getWritableDatabase();
	}
	
	public void close() {
		db.close();
	}

	public long insertCelengan(String tanggal, String kategori, int nominal, String keterangan) {
		ContentValues newValues = new ContentValues();
		newValues.put("tanggal", tanggal);
		newValues.put("kategori", kategori);
		newValues.put("nominal", nominal);
		newValues.put("keterangan", keterangan);
		return db.insert("tb_celengan", null, newValues);
	}

	public Cursor getCelengan(){
		return db.rawQuery("SELECT * FROM tb_celengan ORDER BY id DESC", null); //ASC DESC
	}

	public Cursor getCelenganOne(String id){
		return db.rawQuery("SELECT * FROM tb_celengan WHERE id=?", new String[]{id});
	}

	public long updateCelengan(String id, String kategori, int nominal, String keterangan) {
		ContentValues newValues = new ContentValues();
		newValues.put("id", id);
		newValues.put("kategori", kategori);
		newValues.put("nominal", nominal);
		newValues.put("keterangan", keterangan);
		return db.update("tb_celengan", newValues, "id = " + id, null);
	}

	public long deleteCelengan(String id) {
		return db.delete("tb_celengan", "id = " + id, null);
	}

	public long sum_nominal(String kategori){
		Cursor cur = db.rawQuery("SELECT SUM(nominal) FROM tb_celengan WHERE kategori=?", new String[]{kategori});
		if(cur.moveToFirst()){
			return cur.getLong(0);
		}
		return cur.getLong(0);
	}

}
