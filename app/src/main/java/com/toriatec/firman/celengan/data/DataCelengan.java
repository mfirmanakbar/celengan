package com.toriatec.firman.celengan.data;

/**
 * Created by firmanmac on 12/13/16.
 */

public class DataCelengan {

    String id;
    String kategori;
    String tanggal;

    public String getKeterangan() {
        return keterangan;
    }

    public void setKeterangan(String keterangan) {
        this.keterangan = keterangan;
    }

    String keterangan;
    int nominal;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getKategori() {
        return kategori;
    }

    public void setKategori(String kategori) {
        this.kategori = kategori;
    }

    public String getTanggal() {
        return tanggal;
    }

    public void setTanggal(String tanggal) {
        this.tanggal = tanggal;
    }

    public long getNominal() {
        return nominal;
    }

    public void setNominal(int nominal) {
        this.nominal = nominal;
    }

    public DataCelengan() {
    }

    public DataCelengan(String id, String kategori, String tanggal, int nominal, String keterangan) {
        this.id = id;
        this.kategori = kategori;
        this.tanggal = tanggal;
        this.nominal = nominal;
        this.keterangan = keterangan;
    }
}
