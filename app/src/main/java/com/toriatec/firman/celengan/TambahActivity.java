package com.toriatec.firman.celengan;

import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;

import com.toriatec.firman.celengan.db.DbCelengan;
import com.toriatec.firman.celengan.util.NumberTextWatcher;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class TambahActivity extends AppCompatActivity {

    private EditText mtxtNominal, mKeterangan;
    private String kategori="";
    private String formattedDate;
    private RadioButton mrbPemasukan, mrbPengeluaran;
    private ImageView img1, img2, img3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tambah);

        mtxtNominal = (EditText) findViewById(R.id.txtNominal);
        mKeterangan = (EditText) findViewById(R.id.txtKet);

        Calendar c = Calendar.getInstance();
        System.out.println("Current time => "+c.getTime());
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        formattedDate = df.format(c.getTime());

        mtxtNominal.addTextChangedListener(new NumberTextWatcher(mtxtNominal));

        mrbPemasukan = (RadioButton) findViewById(R.id.rbPemasukan);
        mrbPengeluaran = (RadioButton) findViewById(R.id.rbPengeluaran);
        img1 = (ImageView) findViewById(R.id.imgTambah1);
        img2 = (ImageView) findViewById(R.id.imgTambah2);
        img3 = (ImageView) findViewById(R.id.imgTambah3);

    }

    public void onRadioButtonClicked(View view) {
        boolean checked = ((RadioButton) view).isChecked();
        switch(view.getId()) {
            case R.id.rbPemasukan:
                if (checked)
                    kategori = "Pemasukan";
                    img1.setColorFilter(ContextCompat.getColor(getApplicationContext(),R.color.colorGreen));
                    img2.setColorFilter(ContextCompat.getColor(getApplicationContext(),R.color.colorGreen));
                    img3.setColorFilter(ContextCompat.getColor(getApplicationContext(),R.color.colorGreen));
                    mrbPemasukan.setError(null);
                    mrbPengeluaran.setError(null);
                break;
            case R.id.rbPengeluaran:
                if (checked)
                    kategori = "Pengeluaran";
                    img1.setColorFilter(ContextCompat.getColor(getApplicationContext(),R.color.colorRed));
                    img2.setColorFilter(ContextCompat.getColor(getApplicationContext(),R.color.colorRed));
                    img3.setColorFilter(ContextCompat.getColor(getApplicationContext(),R.color.colorRed));
                    mrbPemasukan.setError(null);
                    mrbPengeluaran.setError(null);
                break;
        }
    }

    public void onBtnNominalClear(View view){
        mtxtNominal.setText("");
    }

    public void onBtnCancelClicked(View view){ finish();}

    public void onBtnSaveClicked(View view){

        String saldo_now = MainActivity.saldo_now;
        int saldo_now_int = Integer.valueOf(saldo_now);
        Log.d("saldo_now_fix", String.valueOf(saldo_now_int));
        String nominalnyas = mtxtNominal.getText().toString();//.replace(".", "");
        String nominalnyass = nominalnyas.replace(".", "");
        nominalnyass = nominalnyass.replace(",","");
        Log.d("pess",nominalnyas + " & " + nominalnyass);

        if(nominalnyass.equals("")){
            mtxtNominal.setError("Nominal kosong!");
        }else if (kategori.isEmpty()){
            mrbPemasukan.setError("Pilih kategori!");
            mrbPengeluaran.setError("Pilih kategori!");
        }
        else {
            Integer nominal_long=0;
            if(nominalnyass!=""){
                nominal_long= Integer.valueOf(nominalnyass);
            }
            if(nominal_long>saldo_now_int && kategori == "Pengeluaran"){
                Log.e("validasi", "tes");
                mtxtNominal.setError("Celengan kamu tidak cukup untuk diambil sebesar" +nominalnyass+" !");
            }else {
                int nominalnya_int = Integer.valueOf(nominalnyass);
                save(formattedDate, kategori, nominalnya_int, mKeterangan.getText().toString());
            }
        }

    }

    private void save(final String formattedDates, final String kategoris, final int nominalnya, final String keterangan) {
        //Log.d("hani", "masuk dulu" + formattedDates + kategoris + nominalnya + keterangan);
        DbCelengan db = new DbCelengan(getApplicationContext());
        db.open();
        db.insertCelengan(formattedDates, kategoris, nominalnya, keterangan);
        db.close();
        finish();
    }

}
