package com.toriatec.firman.celengan.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;

public class DbOpenHelper extends SQLiteOpenHelper {

	private static final int DATABASE_VERSION = 1;
	private static final String DATABASE_NAME = "mymutabaah.db";

	private static final String TABLE_CELENGAN_CREATE = "CREATE TABLE tb_celengan (id INTEGER PRIMARY KEY AUTOINCREMENT, tanggal TEXT, kategori TEXT, nominal INTEGER, keterangan TEXT)";
	private static final String TABLE_LOCK_CREATE = "CREATE TABLE tb_lock (id_lock INTEGER PRIMARY KEY AUTOINCREMENT, password TEXT)";

	public DbOpenHelper(Context context, String name, CursorFactory factory, int version) {
		super(context, DATABASE_NAME, factory, DATABASE_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL(TABLE_CELENGAN_CREATE);
		db.execSQL(TABLE_LOCK_CREATE);

		//Contoh Insert
		/*ContentValues cv = new ContentValues();
		cv.put("idmupi", "TES");
		db.insert("tb_favorite", null, cv);*/
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVer, int newVer) {
		db.execSQL("DROP TABLE IF EXISTS tb_celengan");
		db.execSQL("DROP TABLE IF EXISTS tb_lock");
		onCreate(db);
	}

}
