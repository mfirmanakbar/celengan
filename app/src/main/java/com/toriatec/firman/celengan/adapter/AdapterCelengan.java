package com.toriatec.firman.celengan.adapter;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.toriatec.firman.celengan.R;
import com.toriatec.firman.celengan.data.DataCelengan;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Locale;

/**
 * Created by firmanmac on 12/13/16.
 */

public class AdapterCelengan extends RecyclerView.Adapter<AdapterCelengan.MyViewHolder> {

    private ArrayList<DataCelengan> dataSet;
    Context contexts;

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        TextView textViewTanngal;
        TextView textViewNominal;
        ImageView imgVw;

        public MyViewHolder(View itemView) {
            super(itemView);
            this.textViewTanngal = (TextView) itemView.findViewById(R.id.txtTgl_celengan_item);
            this.textViewNominal = (TextView) itemView.findViewById(R.id.txtNominal_celengan_item);
            this.imgVw = (ImageView) itemView.findViewById(R.id.img_celengan_item);
        }
    }

    public AdapterCelengan(ArrayList<DataCelengan> data) {
        this.dataSet = data;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.celengan_item, parent, false);



        MyViewHolder myViewHolder = new MyViewHolder(view);
        contexts = parent.getContext();
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        TextView textViewTgl = holder.textViewTanngal;
        TextView textViewNom = holder.textViewNominal;
        ImageView imgV = holder.imgVw;

        String getKategori = dataSet.get(position).getKategori();

        //Log.d("hani_dapet", dataSet.get(position).getId() +" - "+ dataSet.get(position).getTanggal() +" - "+ dataSet.get(position).getKategori() +" - "+ dataSet.get(position).getNominal());

        String noms = "Rp "+ NumberFormat.getInstance(Locale.GERMANY).format(Double.parseDouble(String.valueOf(dataSet.get(position).getNominal())));
        textViewTgl.setText(dataSet.get(position).getTanggal());
        textViewNom.setText(noms);
        //textViewNom.setText(String.valueOf(dataSet.get(position).getNominal()));

        if(getKategori.equals("Pemasukan")){
            imgV.setColorFilter(ContextCompat.getColor(contexts,R.color.colorGreen));
        }else if (getKategori.equals("Pengeluaran")){
            imgV.setColorFilter(ContextCompat.getColor(contexts,R.color.colorRed));
        }
    }

    @Override
    public int getItemCount() {
        return dataSet.size();
    }

}
